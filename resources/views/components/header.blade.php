<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
  <div class="container">
    <a class="navbar-brand" href="#"><img src="{{ asset('images/header-logo.png') }}" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item {{ (request()->segment(1) == '') ? 'active' : '' }}">
          <a class="nav-link" href="/">Home
          </a>
        </li>
        <li class="nav-item {{ (request()->segment(2) == 'cars') ? 'active' : '' }}">
          <a class="nav-link" href="{{ url('vehicles/cars') }}">Cars</a>
        </li>
        <li class="nav-item {{ (request()->segment(2) == 'bikes') ? 'active' : '' }}">
          <a class="nav-link" href="{{ url('vehicles/bikes') }}">Bikes</a>
        </li>
        <li class="nav-item {{ (request()->segment(1) == 'contact-us') ? 'active' : '' }}">
          <a class="nav-link" href="{{ url('contact-us') }}">Contact Us</a>
        </li>
      </ul>
    </div>
  </div>
</nav>