@extends('layouts.app')

@section('content')

<!-- Page Content -->
    <!-- Items Starts Here -->
    <div class="featured-page">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <div class="section-heading">
              <div class="line-dec"></div>
              <h1>Vehicles</h1>
            </div>
          </div>
          <div class="col-md-8 col-sm-12">
            <div id="filters" class="button-group">
              <button class="btn btn-primary brandBtn" data-id="" data-filter=".new">All</button>
              @if(count($brands) > 0)
                @foreach($brands as $brand)
                  <button class="btn btn-primary brandBtn" data-id="{{$brand->id}}" data-filter=".new">{{$brand->title}}</button>
                @endforeach
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  
    <div class="featured container no-gutter">
        <div class="row posts">
            @if(count($vehicles) > 0)
              @foreach($vehicles as $vehicle)
                <div id="{{$vehicle->id}}" brandId="{{$vehicle->brandId}}" class="item new col-md-4">
                  <a href="{{ url('vehicle').'/'.$vehicle->id }}">
                    <div class="featured-item">
                      <img src="{{ url('storage').'/'.$vehicle->imagePath }}" alt="{{$vehicle->title}}-image" width="300px" height="150px">
                      <h4>{{$vehicle->title}}</h4>
                      <h6>${{number_format($vehicle->price,2)}}</h6>
                    </div>
                  </a>
                </div>
              @endforeach
            @endif
        </div>
    </div>

    <!-- <div class="page-navigation">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul>
              <li class="current-page"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div> -->
    <!-- Featred Page Ends Here -->

@endsection



@section('page-js')
<!-- <script src="{{ asset('js/custom.js') }}" ></script> -->
<script type="text/javascript">
  $(document).ready(function(){
    $('.brandBtn').click(function(){
      var brandId = $(this).data('id');
      if(brandId == ''){
        $('.item').show();
      }else{
        $('.item').hide();
        $('div[brandId="'+brandId+'"]').show();
      }
    });
  });
</script>
@endsection