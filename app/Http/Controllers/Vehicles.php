<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use App\Brand;

class Vehicles extends Controller
{
    public function getAllVehicles($typeOfCar=''){
        //$vehicles = DB::table('vehicles')->get();
        if($typeOfCar==''){
            $vehicles = Vehicle::all();
            return view("welcome")->with('vehicles',$vehicles);
        }else{
            $data = [];
            if($typeOfCar=='bikes'){
                $brands = Brand::where('vehicleType','bike')->get();
                $brandsArray = $brands->toarray();
                $brandIds = array_column($brandsArray,'id');
                $data['brands'] = $brands;
            }else{
                //$vehicles = Vehicle::all();

                $brands = Brand::where('vehicleType','car')->get();
                $brandsArray = $brands->toarray();
                $brandIds = array_column($brandsArray,'id');
                $data['brands'] = $brands;
            }

            //$vehicles = Vehicle::whereIn('brand',$brandIds)->get();
            $vehicles = Vehicle::select('vehicles.*','brands.title as brand','brands.id as brandId')->join('brands','vehicles.brand','=','brands.id')->whereIn('brand',$brandIds)->get();
            /*return $vehicles;*/
            $data['vehicles'] = $vehicles;
            
            return view("vehicles")->with($data);
        }
        
    }

    public function getVehicle($vehicleId){
        /*echo $vehicleId;exit;*/
        $vehicle = Vehicle::where('id',$vehicleId)->get();
        return view("vehicle")->with('vehicle',$vehicle[0]);
    }
}
