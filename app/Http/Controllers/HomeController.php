<?php

namespace App\Http\HomeController;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getAllVehicles(){
        $vehicles = DB::table('vehicles')->get();
        return View::make("welcome")->with('vehicles',$vehicles);
    }
}
