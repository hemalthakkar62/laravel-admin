-- phpMiniAdmin dump 1.9.160630
-- Datetime: 2021-08-14 11:51:07
-- Host: sweat-social-db.cgdfnfbiwfup.us-west-2.rds.amazonaws.com
-- Database: helios

/*!40030 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagePath` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vehicleType` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES ('1','Toyota','brands/July2021/6puTvv5uy86cHXh3mQQm.png','2021-07-21 11:01:42','2021-07-21 11:01:42','car'),('2','Yamaha','brands/July2021/15haSoCfohHHURTDh0kW.png','2021-07-21 11:03:43','2021-07-21 11:03:43','bike'),('3','Bajaj','brands/August2021/0DELlgWtaTlEeluHZIDH.png','2021-08-14 08:16:18','2021-08-14 08:16:18','bike'),('4','Maruti','brands/August2021/F6dzuANp0lXZJnnweY3I.png','2021-08-14 08:32:42','2021-08-14 08:32:42','car');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES ('1','1','id','number','ID','1','0','0','0','0','0',NULL,'1'),('2','1','name','text','Name','1','1','1','1','1','1',NULL,'2'),('3','1','email','text','Email','1','1','1','1','1','1',NULL,'3'),('4','1','password','password','Password','1','0','0','1','1','0',NULL,'4'),('5','1','remember_token','text','Remember Token','0','0','0','0','0','0',NULL,'5'),('6','1','created_at','timestamp','Created At','0','1','1','0','0','0',NULL,'6'),('7','1','updated_at','timestamp','Updated At','0','0','0','0','0','0',NULL,'7'),('8','1','avatar','image','Avatar','0','1','1','1','1','1',NULL,'8'),('9','1','user_belongsto_role_relationship','relationship','Role','0','1','1','1','1','0','{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}','10'),('10','1','user_belongstomany_role_relationship','relationship','Roles','0','1','1','1','1','0','{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}','11'),('11','1','settings','hidden','Settings','0','0','0','0','0','0',NULL,'12'),('12','2','id','number','ID','1','0','0','0','0','0',NULL,'1'),('13','2','name','text','Name','1','1','1','1','1','1',NULL,'2'),('14','2','created_at','timestamp','Created At','0','0','0','0','0','0',NULL,'3'),('15','2','updated_at','timestamp','Updated At','0','0','0','0','0','0',NULL,'4'),('16','3','id','number','ID','1','0','0','0','0','0',NULL,'1'),('17','3','name','text','Name','1','1','1','1','1','1',NULL,'2'),('18','3','created_at','timestamp','Created At','0','0','0','0','0','0',NULL,'3'),('19','3','updated_at','timestamp','Updated At','0','0','0','0','0','0',NULL,'4'),('20','3','display_name','text','Display Name','1','1','1','1','1','1',NULL,'5'),('21','1','role_id','text','Role','1','1','1','1','1','1',NULL,'9'),('22','4','id','text','Id','1','0','0','0','0','0','{}','1'),('23','4','title','text','Title','0','1','1','1','1','1','{}','3'),('24','4','imagePath','image','ImagePath','0','1','1','1','1','1','{}','4'),('25','4','created_at','timestamp','Created At','0','1','1','1','0','1','{}','5'),('26','4','updated_at','timestamp','Updated At','0','0','0','0','0','0','{}','6'),('27','4','vehicleType','select_dropdown','VehicleType','0','1','1','1','1','1','{\"default\":\"\",\"options\":{\"\":\"Please Select\",\"car\":\"Car\",\"bike\":\"Bike\"}}','2'),('28','7','id','text','Id','1','0','0','0','0','0','{}','1'),('29','7','brand','text','Brand','0','1','1','1','1','1','{}','2'),('30','7','title','text','Title','0','1','1','1','1','1','{}','4'),('31','7','imagePath','image','ImagePath','0','1','1','1','1','1','{}','5'),('32','7','facilities','rich_text_box','Facilities','0','1','1','1','1','1','{}','7'),('33','7','created_at','timestamp','Created At','0','1','1','1','0','1','{}','8'),('34','7','updated_at','timestamp','Updated At','0','0','0','0','0','0','{}','9'),('35','7','vehicle_belongsto_brand_relationship','relationship','brands','0','1','1','1','1','1','{\"model\":\"App\\\\Brand\",\"table\":\"brands\",\"type\":\"belongsTo\",\"column\":\"brand\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"brands\",\"pivot\":\"0\",\"taggable\":\"0\"}','3'),('36','7','price','text','Price','1','1','1','1','1','1','{}','6');
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES ('1','users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','','1','0',NULL,'2021-07-21 10:07:29','2021-07-21 10:07:29'),('2','menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','','1','0',NULL,'2021-07-21 10:07:32','2021-07-21 10:07:32'),('3','roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController','','1','0',NULL,'2021-07-21 10:07:33','2021-07-21 10:07:33'),('4','brands','brands','Brand','Brands','voyager-truck','App\\Brand',NULL,NULL,NULL,'1','0','{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2021-07-21 10:26:07','2021-07-21 10:35:07'),('7','vehicles','vehicles','Vehicle','Vehicles','voyager-basket','App\\Vehicle',NULL,'App\\Http\\Controllers\\ImportVehicles',NULL,'1','0','{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2021-07-21 11:15:00','2021-08-14 11:32:43');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES ('1','1','Dashboard','','_self','voyager-boat',NULL,NULL,'1','2021-07-21 10:08:19','2021-07-21 10:08:19','voyager.dashboard',NULL),('2','1','Media','','_self','voyager-images',NULL,NULL,'5','2021-07-21 10:08:22','2021-07-21 10:08:22','voyager.media.index',NULL),('3','1','Users','','_self','voyager-person',NULL,NULL,'3','2021-07-21 10:08:26','2021-07-21 10:08:26','voyager.users.index',NULL),('4','1','Roles','','_self','voyager-lock',NULL,NULL,'2','2021-07-21 10:08:28','2021-07-21 10:08:28','voyager.roles.index',NULL),('5','1','Tools','','_self','voyager-tools',NULL,NULL,'9','2021-07-21 10:08:32','2021-07-21 10:08:32',NULL,NULL),('6','1','Menu Builder','','_self','voyager-list',NULL,'5','10','2021-07-21 10:08:35','2021-07-21 10:08:35','voyager.menus.index',NULL),('7','1','Database','','_self','voyager-data',NULL,'5','11','2021-07-21 10:08:38','2021-07-21 10:08:38','voyager.database.index',NULL),('8','1','Compass','','_self','voyager-compass',NULL,'5','12','2021-07-21 10:08:40','2021-07-21 10:08:40','voyager.compass.index',NULL),('9','1','BREAD','','_self','voyager-bread',NULL,'5','13','2021-07-21 10:08:43','2021-07-21 10:08:43','voyager.bread.index',NULL),('10','1','Settings','','_self','voyager-settings',NULL,NULL,'14','2021-07-21 10:08:46','2021-07-21 10:08:46','voyager.settings.index',NULL),('11','1','Hooks','','_self','voyager-hook',NULL,'5','13','2021-07-21 10:10:30','2021-07-21 10:10:30','voyager.hooks',NULL),('12','1','Brands','','_self','voyager-truck',NULL,NULL,'15','2021-07-21 10:26:34','2021-07-21 10:26:34','voyager.brands.index',NULL),('13','1','Vehicles','','_self',NULL,NULL,NULL,'16','2021-07-21 11:15:34','2021-07-21 11:15:34','voyager.vehicles.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES ('1','admin','2021-07-21 10:08:17','2021-07-21 10:08:17');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('1','2014_10_12_000000_create_users_table','1'),('2','2014_10_12_100000_create_password_resets_table','1'),('3','2016_01_01_000000_add_voyager_user_fields','1'),('4','2016_01_01_000000_create_data_types_table','1'),('5','2016_05_19_173453_create_menu_table','1'),('6','2016_10_21_190000_create_roles_table','1'),('7','2016_10_21_190000_create_settings_table','1'),('8','2016_11_30_135954_create_permission_table','1'),('9','2016_11_30_141208_create_permission_role_table','1'),('10','2016_12_26_201236_data_types__add__server_side','1'),('11','2017_01_13_000000_add_route_to_menu_items_table','1'),('12','2017_01_14_005015_create_translations_table','1'),('13','2017_01_15_000000_make_table_name_nullable_in_permissions_table','1'),('14','2017_03_06_000000_add_controller_to_data_types_table','1'),('15','2017_04_21_000000_add_order_to_data_rows_table','1'),('16','2017_07_05_210000_add_policyname_to_data_types_table','1'),('17','2017_08_05_000000_add_group_to_settings_table','1'),('18','2017_11_26_013050_add_user_role_relationship','1'),('19','2017_11_26_015000_create_user_roles_table','1'),('20','2018_03_11_000000_add_user_settings','1'),('21','2018_03_14_000000_add_details_to_data_types_table','1'),('22','2018_03_16_000000_make_settings_value_nullable','1'),('23','2019_08_19_000000_create_failed_jobs_table','1');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

DROP TABLE IF EXISTS `models`;
CREATE TABLE `models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand` int(11) DEFAULT NULL,
  `title` varchar(170) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagePath` text COLLATE utf8mb4_unicode_ci,
  `facilities` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `models` DISABLE KEYS */;
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES ('1','1'),('2','1'),('3','1'),('4','1'),('5','1'),('6','1'),('7','1'),('8','1'),('9','1'),('10','1'),('11','1'),('12','1'),('13','1'),('14','1'),('15','1'),('16','1'),('17','1'),('18','1'),('19','1'),('20','1'),('21','1'),('22','1'),('23','1'),('24','1'),('25','1'),('26','1'),('27','1'),('28','1'),('29','1'),('30','1'),('31','1'),('32','1'),('33','1'),('34','1'),('35','1'),('36','1');
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES ('1','browse_admin',NULL,'2021-07-21 10:08:53','2021-07-21 10:08:53'),('2','browse_bread',NULL,'2021-07-21 10:08:54','2021-07-21 10:08:54'),('3','browse_database',NULL,'2021-07-21 10:08:56','2021-07-21 10:08:56'),('4','browse_media',NULL,'2021-07-21 10:08:58','2021-07-21 10:08:58'),('5','browse_compass',NULL,'2021-07-21 10:09:00','2021-07-21 10:09:00'),('6','browse_menus','menus','2021-07-21 10:09:02','2021-07-21 10:09:02'),('7','read_menus','menus','2021-07-21 10:09:04','2021-07-21 10:09:04'),('8','edit_menus','menus','2021-07-21 10:09:06','2021-07-21 10:09:06'),('9','add_menus','menus','2021-07-21 10:09:08','2021-07-21 10:09:08'),('10','delete_menus','menus','2021-07-21 10:09:09','2021-07-21 10:09:09'),('11','browse_roles','roles','2021-07-21 10:09:11','2021-07-21 10:09:11'),('12','read_roles','roles','2021-07-21 10:09:13','2021-07-21 10:09:13'),('13','edit_roles','roles','2021-07-21 10:09:15','2021-07-21 10:09:15'),('14','add_roles','roles','2021-07-21 10:09:17','2021-07-21 10:09:17'),('15','delete_roles','roles','2021-07-21 10:09:19','2021-07-21 10:09:19'),('16','browse_users','users','2021-07-21 10:09:21','2021-07-21 10:09:21'),('17','read_users','users','2021-07-21 10:09:23','2021-07-21 10:09:23'),('18','edit_users','users','2021-07-21 10:09:25','2021-07-21 10:09:25'),('19','add_users','users','2021-07-21 10:09:26','2021-07-21 10:09:26'),('20','delete_users','users','2021-07-21 10:09:28','2021-07-21 10:09:28'),('21','browse_settings','settings','2021-07-21 10:09:29','2021-07-21 10:09:29'),('22','read_settings','settings','2021-07-21 10:09:31','2021-07-21 10:09:31'),('23','edit_settings','settings','2021-07-21 10:09:33','2021-07-21 10:09:33'),('24','add_settings','settings','2021-07-21 10:09:35','2021-07-21 10:09:35'),('25','delete_settings','settings','2021-07-21 10:09:36','2021-07-21 10:09:36'),('26','browse_hooks',NULL,'2021-07-21 10:10:32','2021-07-21 10:10:32'),('27','browse_brands','brands','2021-07-21 10:26:21','2021-07-21 10:26:21'),('28','read_brands','brands','2021-07-21 10:26:23','2021-07-21 10:26:23'),('29','edit_brands','brands','2021-07-21 10:26:25','2021-07-21 10:26:25'),('30','add_brands','brands','2021-07-21 10:26:27','2021-07-21 10:26:27'),('31','delete_brands','brands','2021-07-21 10:26:29','2021-07-21 10:26:29'),('32','browse_vehicles','vehicles','2021-07-21 11:15:19','2021-07-21 11:15:19'),('33','read_vehicles','vehicles','2021-07-21 11:15:21','2021-07-21 11:15:21'),('34','edit_vehicles','vehicles','2021-07-21 11:15:24','2021-07-21 11:15:24'),('35','add_vehicles','vehicles','2021-07-21 11:15:26','2021-07-21 11:15:26'),('36','delete_vehicles','vehicles','2021-07-21 11:15:28','2021-07-21 11:15:28');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('1','admin','Administrator','2021-07-21 10:08:48','2021-07-21 10:08:48'),('2','user','Normal User','2021-07-21 10:08:51','2021-07-21 10:08:51');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES ('1','site.title','Site Title','Site Title','','text','1','Site'),('2','site.description','Site Description','Site Description','','text','2','Site'),('3','site.logo','Site Logo','','','image','3','Site'),('4','site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text','4','Site'),('5','admin.bg_image','Admin Background Image','','','image','5','Admin'),('6','admin.title','Admin Title','Voyager','','text','1','Admin'),('7','admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text','2','Admin'),('8','admin.loader','Admin Loader','','','image','3','Admin'),('9','admin.icon_image','Admin Icon Image','','','image','4','Admin'),('10','admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text','1','Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1','1','Admin','admin@admin.com','users/default.png',NULL,'$2y$10$/fngjfxhZHxidLQLOzPvVOYOk6TgkQZsqlQmTz1A7/rEywyD9NoGW','pXMgFV4DAYrrJphw6aydGCfF4oWKcXJYj2A8jyQtqbPf5fGSx5iM4L6EiRkd',NULL,'2021-07-21 10:12:25','2021-07-21 10:12:32');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand` int(11) DEFAULT NULL,
  `title` varchar(170) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagePath` text COLLATE utf8mb4_unicode_ci,
  `facilities` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES ('1','2','FZ','vehicles/July2021/6GpjMNq5HzuvaF7GZ0G7.jpeg','<p>Test Text</p>','2021-07-21 11:22:00','2021-08-14 06:40:28','80'),('2','1','Corola','vehicles/July2021/57M412cDaHcMcjStJoCh.jpg','<p>Corola Text</p>','2021-07-21 11:26:00','2021-08-14 06:39:24','1000'),('3','2','R15','vehicles/August2021/x3GDm2W44tZbdohUUkio.png','<p>R15 Facilities</p>','2021-08-14 06:35:35','2021-08-14 06:35:35','100'),('4','1','Innova','vehicles/August2021/n6W6oTudjjOzugNCFNGn.png','<p>Innova TeXt</p>','2021-08-14 06:43:25','2021-08-14 06:43:25','1300'),('5','1','Fortuner','vehicles/August2021/GBLSznL5qeOnVOtUN4m3.jpg','<p>Fortuner Facilities</p>','2021-08-14 06:47:47','2021-08-14 06:47:47','1500'),('6','4','S-Presso','vehicles/August2021/24HGONujx3a8ZobIliOb.png','<p>S-Presso Facilities</p>','2021-08-14 09:45:09','2021-08-14 09:45:09','900'),('7','4','Ertiga','vehicles/August2021/rH2v9T5jdwsUC7AO0n5A.jpg','<p>Ertiga Facilities</p>','2021-08-14 09:46:49','2021-08-14 09:46:49','800'),('8','3','Pulsar','vehicles/August2021/wzWMdhiFKPr9FESP9EmR.png','<p>Pulsar Facilities</p>','2021-08-14 09:50:51','2021-08-14 09:50:51','100'),('9','3','Discover','vehicles/August2021/IhfcIZzFfzsbl4rQLbwe.png','<p>Descover Facilities</p>','2021-08-14 09:52:56','2021-08-14 09:52:56','60');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;


-- phpMiniAdmin dump end
