##
To check this project, please follow below steps

1) Run Command **composer install** to install all dependencies

2) Use phpmyadmin or other mysql client to create database helios

3) Import sql/import.sql file to newely create database

4) Copy env.example to .env and update following details

	Edit .env file for following parameters

	APP_URL=http://localhost:8000  

	DB_CONNECTION=mysql  
	DB_HOST=localhost  
	DB_PORT=3306  
	DB_DATABASE=helios  
	DB_USERNAME=root  
	DB_PASSWORD=  

	set App url same as mentioned above, you just need to change host,username and password

5) Generate Key for laravel project using Command **php artisan key:generate**

6) Run command **php artisan storage:link** to update storage links

7) Run command **php artisan serve** to start running project

8) In browse open url: **http://localhost:8000/** to access front end  
	And Url: **http://localhost:8000/admin/** to access admin panel  



	Admin Credentials:  
		Username: admin@admin.com  
		Password : 123456789


# From Admin Portal
9) Access brands section to add new vehicle brands

10) Access vehicles section to add new vehicles

#Fron Front End

You can check all vehicles, filter them based on vehicle type and brand, can also check vehicle details page by clicking vehicle