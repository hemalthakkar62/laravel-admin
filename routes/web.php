<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Vehicles;
use App\Http\Controllers\ImportVehicles;
use TCG\Voyager\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'Vehicles@getAllVehicles');
Route::get('/', [Vehicles::class,'getAllVehicles']);
Route::get('/vehicles/{typeOfCar}', [Vehicles::class,'getAllVehicles']);
Route::get('/contact-us', function () {
    return view('contact-us');
});
Route::get('/vehicle/{vehicleId}', [Vehicles::class,'getVehicle']);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::post('vehicles/import', ['uses' => 'ImportVehicles@import', 'as' => 'import']);
});
